function [ret] = compute(ctl_file_path, outpath)

[fid,message]=fopen(ctl_file_path);
message;
filnam=textscan(fid,'%s');
fclose(fid)

for i=1:length(filnam{1})
	fprintf('%d / %d =============================', i, length(filnam{1}));
	infile = filnam{1}{i};
	C=strsplit(filnam{1}{i}, '/');
	tmp = strcat(outpath, C(end))
	tmpfile = char(strrep(tmp,'.wav','.tmp'));
	outfile = char(strrep(tmpfile,'.tmp','.mfc'));

	if exist(outfile)==2
		disp('skip');
	else
		disp('compute');
		PNCC_IEEETran(tmpfile, infile);
		convert(tmpfile,outfile);
		delete(tmpfile);
	end
end
fprintf('\n\n===========================================\n');
fprintf('compute finish: out:%s\n', path);
fprintf('===========================================\n\n');
end
