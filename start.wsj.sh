#!/bin/bash

db=/raid2/sykim/wsj0
ctl=$db/wsj0.wav.flist

function run() {
echo "Start computing PNCC: $1 $2"
noise=$1
snr=$2

out=$db.$noise.$snr/
runscriptm.sh compute $ctl $out 
echo "Run computing pncc: $1 $2"
sleep 1;
}

#runscriptm.sh compute $ctl $db/ $db.pncc/

run white 00
run white 05
run white 10
run white 15
run white 20
run street 00
run street 05
run street 10
run street 15
run street 20
run music 00
run music 05
run music 10
run music 15
run music 20
