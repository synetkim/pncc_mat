#!/bin/bash

function run() {
echo "Start computing PNCC: $1 "

out=$1
ctl=$2

mkdir -p $out
runscriptm.sh compute $ctl $out 
}

run out.beamform.bus/ /data-local/sykim/tool/chime3/multi/bus.flist
run out.beamform.str/ /data-local/sykim/tool/chime3/multi/str.flist
run out.beamform.caf/ /data-local/sykim/tool/chime3/multi/caf.flist
run out.beamform.ped/ /data-local/sykim/tool/chime3/multi/ped.flist
