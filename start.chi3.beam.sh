#!/bin/bash

db=/data-local/sykim/database/chime3_5ch_beamform
ctl=/data-local/sykim/database/chime3_5ch_beamform/beam.flist

function run() {
echo "Start computing PNCC: $1 "
out=$1
mkdir -p $out
runscriptm.sh compute $ctl $out 
}

run out.isolated_beamform/
