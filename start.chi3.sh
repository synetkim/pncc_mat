#!/bin/bash

db=/data-local/sykim/database/enhanced
ctl=/data-local/sykim/database/enhanced.flist

function run() {
echo "Start computing PNCC: $1 "
out=$1
mkdir -p $out
runscriptm.sh compute $ctl $out 
sleep 1;
}

#runscriptm.sh compute $ctl $db/ $db.pncc/
run out/
