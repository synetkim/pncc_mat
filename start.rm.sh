#!/bin/bash

db=/data-local/sykim/database/rm
ctl=$db.clean.100/rm.ssf.wav.flist

function run() {
echo "Start computing PNCC: $1 $2"
noise=$1
snr=$2

out=$db.$noise.$snr/
runscriptm.sh compute_ssf $ctl $out 
echo "Run computing pncc: $1 $2"
sleep 1;
}

#runscriptm.sh compute $ctl $db/ $db.pncc/
run reverb 0.3
run reverb 0.6
#run reverb 1.0
run white 00
run white 05
run white 10
run white 15
run white 20
run street 00
run street 05
run street 10
run street 15
run street 20
run music 00
run music 05
run music 10
run music 15
run music 20
