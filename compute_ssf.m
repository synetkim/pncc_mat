function [ret] = compute(ctl_file_path, path)
disp(ctl_file_path);
disp(path);
[fid,message]=fopen(ctl_file_path);
message;
filnam=textscan(fid,'%s');
fclose(fid)

for i=1:length(filnam{1})
	fprintf('%d / %d =============================', i, length(filnam{1}));
	infile = strcat(path, filnam{1}{i})
	tmpfile = strcat(path, filnam{1}{i});
	tmpfile = strcat(tmpfile(1:end-4),'.tmp')
	outfile = strcat(tmpfile(1:end-4),'.pn3')

	if exist(outfile)==2
		disp('skip');
	else
		disp('compute');
		PNCC_IEEETran_wav(tmpfile, infile);
		convert(tmpfile,outfile);
		delete(tmpfile);
	end
end
fprintf('\n\n===========================================\n');
fprintf('compute finish: out:%s\n', path);
fprintf('===========================================\n\n');
end
