
function [ret ] = convert(in, out)
fin = fopen(in,'r','ieee-be');
x=fread(fin,inf,'int');

%write it out
fout = fopen(out,'w','ieee-le');
fwrite(fout,x,'int');

fclose(fin)
fclose(fout)
end
